using Microsoft.VisualStudio.TestTools.UnitTesting;
using Zen.Hr.Logic;
using Moq;

/*Have used Moq as mocking framework*/

namespace Zen.Hr.Logic.Tests
{
    [TestClass]
    public class UserServiceTests
    {
        private UserService concern;

        [TestMethod]
        public void Successfully_Returns_All_Users_When_Passing_In_False()
        {
            // unit test goes here

            //arrange
            var mock = new Mock<IUserDataAccess>();
            concern = new UserService(mock.Object);
            bool isActive = false;

            //act
            concern.GetUsers(isActive);

            //assert
            mock.Verify(d => d.GetAllUsers(), Times.Once);
        }

        [TestMethod]
        public void Successfully_Returns_All_Active_Users_When_Passing_In_True()
        {
            // unit test goes here

            //arrange
            var mock = new Mock<IUserDataAccess>();
            concern = new UserService(mock.Object);
            bool isActive = true;

            //act
            concern.GetUsers(isActive);

            //assert
            mock.Verify(d => d.GetAllActiveUsers(), Times.Once);

        }
    }
}

